new Vue ({
    el : '#vue-app',
    data : {
        name : 'Shahin',
        job : 'Front End Developer',
        website : 'www.mysports.com',
        websiteTag : '<a href="www.mysports.com">My Site</a>'
    },
    methods : {
        greetings : function (time) {
            return 'Good ' + time + ' ' + this.name;
        }
    }
});